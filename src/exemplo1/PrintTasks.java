/**
 * Exemplo1: Programacao com threads
 * Autor: Bruno Russi Lautenschlager
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;

/**
 *
 * @author Bruno Russi Lautenschlager
 */
public class PrintTasks implements Runnable {
    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();
    private static int _ID;
    
    
    public PrintTasks(String name){
        taskName = name;
        
        //Tempo aleatorio entre 0 e 1 segundos
        sleepTime = generator.nextInt(1000); //milissegundos
        
        _ID++;
        System.out.printf("Id:" + _ID + "\n");
    }
    
    public void run(){
        try{
            //Estado de ESPERA SINCRONIZADA
            //Nesse ponto, a thread perde o processador, e permite que
            //outra thread execute
            if(_ID%2 != 0){
                System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
                Thread.sleep(sleepTime);
            }
            
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
    }

}
