/**
 * Exemplo1: Programacao com threads
 * Autor: Bruno Russi Lautenschlager
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Bruno Russi Lautenschlager
 */
public class Exemplo1  {
    public static void main(String [] args){
        System.out.println("Inicio da criacao das threads.");
        
        ExecutorService threadPool = Executors.newCachedThreadPool();
        
        for (int i = 1; i <= 20; i++) {
            
            //Cria cada thread com um novo runnable selecionado
            Thread t = new Thread(new PrintTasks("thread" + i));

            //Inicia as threads, e as coloca no estado EXECUTAVEL
            threadPool.execute(t);
//            t.start();
        }

        System.out.println("Threads criadas");
        
    }
}
